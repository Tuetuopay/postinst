#!/bin/bash

[[ $(whoami) != "root" ]] && echo "Run me as root è_é" && exit

# Determining distrib
if [[ $(uname -r | grep -E el\[0-9\]+) != "" ]]; then
	distribution_name=CentOS
	distribution_version=$(uname -r | sed -E s/.*el\(\[0-9\]+\).*/\\1/g)
elif [[ $(uname -v | grep -E \+deb\[0-9\]+u\[0-9\]+) != "" ]]; then
	distribution_name=Debian
	distribution_version=$(uname -v | sed -E s/.*\+deb\(\[0-9\]+\)u\[0-9\].*/\\1/g)
elif [[ $(uname -a | grep -i ubuntu) != "" ]]; then          # At least, Ubuntu is easy to detect
	distribution_name=Ubuntu
	distribution_version=$(lsb_release -r | sed -E s/^\[^0-9\]*\(\[0-9.\]\)/\1/g)
elif [[ $(uname -r | grep -E fc\[0-9\]+) != "" ]]; then
	distribution_name=Fedora
	distribution_version=$(uname -r | sed -E s/.*fc\(\[0-9\]+\).*/\\1/g)
else
	echo "Unsupported distro :/"
	exit
fi

echo "Detected $distribution_name version $distribution_version."

# Installing fish
read -p "Install fish, a better shell? (Y/n) " do_install
if [[ -z $do_install || $do_install =~ ^y(es)?$ ]]; then
	if [[ " CentOS Fedora " =~ " $distribution_name " ]]; then
		# Let's assume we have a compatible system. I'm too lazy ATM to properly think this check
		cd /etc/yum.repos.d && \
			wget http://download.opensuse.org/repositories/shells:fish:release:2/${distribution_name}_${distribution_version}/shells:fish:release:2.repo && \
			yum install -y --disablerepo=epel fish
	elif [[ $distribution_name == Debian ]]; then
		echo 'deb http://download.opensuse.org/repositories/shells:/fish:/release:/2/Debian_${distribution_version}.0/ /' >> /etc/apt/sources.list.d/fish.list
		apt-get update && apt-get install -y fish

		read -p "Add fish's repo key to apt? (y/N) " do_add
		if [[ ! -z $do_add && $do_install =~ ^y(es)?$ ]]; then
			# Yeah dirty but heh, the default option is "no", and it's not even mandatory!
			curl http://download.opensuse.org/repositories/shells:fish:release:2/Debian_${distribution_version}.0/Release.key | apt-key add -
		fi
	elif [[ $distribution_name == Ubuntu ]]; then
		apt-add-repository -y ppa:fish-shell/release-2
		apt-get update && apt-get install -y fish
	fi

	read -p "Make fish your default shell? (Y/n) " do_default
	[[ -z $do_default || $do_default =~ ^y(es)?$ ]] && echo $(which fish) >> /etc/shells && chsh -s $(which fish)
fi

read -p "Install Docker? (Y/n) " do_install
if [[ -z $do_install || $do_install =~ ^y(es)?$ ]]; then
	if [[ " CentOS Fedora " =~ " $distribution_name " ]]; then
		tee /etc/yum.repos.d/docker.repo <<-'EOF'
[dockerrepo]
name=Docker Repository
baseurl=https://yum.dockerproject.org/repo/main/centos/$releasever/
enabled=1
gpgcheck=1
gpgkey=https://yum.dockerproject.org/gpg
EOF
		yum install -y docker-engine
		service docker start
		groupadd docker
		read -p "User to add to the docker group : " docker_user
		if [[ ! -z $docker_user ]]; then
			usermod -aG docker $docker_user
		fi
		chkconfig docker on
	fi
fi

